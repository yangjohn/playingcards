#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

// Rank enum
enum Rank
{
	TWO = 2, 
	THREE, 
	FOUR, 
	FIVE, 
	SIX, 
	SEVEN,
	EIGHT,
	NINE, 
	TEN,  
	JACK,
	QUEEN, 
	KING,
	ACE = 14

};

//Suit enum
enum Suit
{
	SPADE, 
	HEART, 
	DIAMOND, 
	CLUB
};

// Card Structure
struct Card
{
	// members
	Rank rank;
	Suit suit;
};

int main()
{
	_getch();
	return 0;
}